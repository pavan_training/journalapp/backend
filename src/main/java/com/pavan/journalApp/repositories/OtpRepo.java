package com.pavan.journalApp.repositories;

import com.pavan.journalApp.models.Otp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtpRepo extends JpaRepository<Otp, Long> {

    Otp findByEmail(String email);

    Otp findByOtp(String otp);

}
