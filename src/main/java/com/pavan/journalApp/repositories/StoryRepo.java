package com.pavan.journalApp.repositories;

import com.pavan.journalApp.models.Stories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface StoryRepo extends JpaRepository<Stories, Long> {

    public List<Stories> findByUserEmail(String email);

    @Query(value = "from Stories t where createDate BETWEEN :startDate AND :endDate AND email=:email")
    public List<Stories> getStoriesBetweenDates(@Param("startDate")Date startDate,@Param("endDate") Date endDate,@Param("email") String email);
}
