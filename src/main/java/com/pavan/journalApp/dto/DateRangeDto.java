package com.pavan.journalApp.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DateRangeDto {
    private Date fromDate;
    private Date toDate;
    private String email;
}
