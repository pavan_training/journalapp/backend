package com.pavan.journalApp.utils;

public class CountingValleys {

    // Complete the countingValleys function below.
    static int countValleys(int n, String s) {
        int answer = 0;
        int level = 0;

        for(int i= 0;i<n;i++){      // "DDUU DDUDUU UD"
            char c = s.charAt(i);   // level=         answer=2

            if(c=='U'){
                level++;
            }else{
                level--;
            }

            if(c=='U' && level==0){
                answer++;
            }

        }

        return answer;
    }


    public static void main(String[] args) {
        CountingValleys countingValleys = new CountingValleys();
        int answer = countingValleys.countValleys(12,"DDUUDDUDUUDU");

        /* DDUU DDUDUU UD = 2

                      /\
            \  /\    /
             \/  \/\/

            D =1, U=1

            count  = 2
        */




        System.out.println(answer);
    }
}
