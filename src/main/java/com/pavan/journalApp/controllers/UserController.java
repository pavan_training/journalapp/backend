package com.pavan.journalApp.controllers;

import com.pavan.journalApp.dto.LoginDto;
import com.pavan.journalApp.dto.ResetPasswordDto;
import com.pavan.journalApp.models.Otp;
import com.pavan.journalApp.models.Response;
import com.pavan.journalApp.models.User;
import com.pavan.journalApp.repositories.OtpRepo;
import com.pavan.journalApp.repositories.UserRepo;
import com.pavan.journalApp.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.Random;


@RestController
public class UserController {

    @Autowired
    UserRepo userRepo;

    @Autowired
    OtpRepo otpRepo;

    @Autowired
    private JavaMailSender javaMailSender;

    @PostMapping("/addUser")
    public Response<User> addUser(@RequestBody @Valid User user) {

        User dbUser = userRepo.save(user);

        return new Response<>(Constants.SUCCESS, "User Addedd successfully", dbUser);
    }

    @GetMapping("/getUser/{id}")
    public Response<User> getUser(@PathVariable("id") Long id) {

        Optional<User> result = userRepo.findById(id);
        Response res;

        if (result.isPresent()) {

            res = new Response<User>(Constants.SUCCESS, "user added successfully", result.get());

        } else {
            res = new Response(Constants.FAILED, "User not found", null);
        }

        return res;

    }


    @DeleteMapping("/deleteUser/{id}")
    public Response<User> deleteUser(@PathVariable(value = "id") Long id) {

        User user = userRepo.findById(id).get();

        if (user == null) {
            return new Response<User>(Constants.FAILED, "user not found", user);
        } else {
            userRepo.deleteById(id);
            return new Response<User>(Constants.SUCCESS, "User deleted successfully", user);
        }
    }

    @PutMapping("/updateUser")
    public Response<User> updateUser(@RequestBody User user) {

        Long id = user.getId();
        User db = userRepo.findById(id).get();

        if (db != null) {
            userRepo.save(user);
        }

        return new Response<>(Constants.SUCCESS, "user updated successfully", user);
    }

    @PostMapping("/login")
    public Object login(@RequestBody LoginDto loginDto) {


        User dbuser = userRepo.findByEmailAndPassword(loginDto.getUserName(), loginDto.getPassword());

        if (dbuser == null)
            return new Response<LoginDto>(Constants.FAILED, "please check entered username and password", loginDto);

        return new Response<User>(Constants.SUCCESS, "login successfull", dbuser);
    }

    @GetMapping("/generateOtp")
    public Response<Otp> generateOtp(@RequestParam("email") String email) {

        // is email registered or not
        User user = userRepo.findByEmail(email);
        if (user == null) {
            return new Response<>(Constants.FAILED, "Email not registered please check and try again", null);
        }

        // If email already present in OTP table then update the OTP otherwise add a new record
        Otp otp = otpRepo.findByEmail(email);
        if (otp == null) {
            otp = new Otp();
        }

        otp.setEmail(email);
        String str = new String(randomPassword(6));
        otp.setOtp(str);

        Otp newOtp = otpRepo.save(otp);

        // send email
        SimpleMailMessage msg = new SimpleMailMessage();
//        msg.setTo("pavan@yopma@gmail.com", "to_2@gmail.com", "to_3@yahoo.com");
        msg.setTo(email);
        msg.setSubject("Reset password JournalApp");
        msg.setText("Please use below OTP to reset your password. OTP: "+newOtp.getOtp());
        javaMailSender.send(msg);


        return new Response<Otp>(Constants.SUCCESS, "OTP generated successfully", newOtp);
    }


    @PostMapping("/resetPassword")
    public Response<String> resetPassword(@RequestBody ResetPasswordDto req) {

        String reqOtp = req.getOtp();
        String reqNewPassword = req.getNew_password();
        String reqRePassword = req.getRe_password();

        // OTP -> EMAIL -> USER -> Password -> remove OTP


        // Get emailId using OTP
        Otp dbOtp = otpRepo.findByOtp(reqOtp);

        if (dbOtp == null) {
            return new Response<>(Constants.FAILED, "Invalid OTP", "");
        }

        String email = dbOtp.getEmail();

        // validate new password and re_password should same
        if (reqNewPassword.equals(reqRePassword)) {
            User user = userRepo.findByEmail(email);
            user.setPassword(reqNewPassword);
            userRepo.save(user);
        }

        dbOtp.setOtp(null);
        otpRepo.save(dbOtp);

        return new Response<>(Constants.SUCCESS, "Password updated successfully", "");
    }


    static char[] randomPassword(int len) {
        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String symbols = "!@#$%^&*_=+-/.?<>)";


        String values = Capital_chars + Small_chars +
                numbers + symbols;

        // Using random method
        Random rndm_method = new Random();

        char[] password = new char[len];

        for (int i = 0; i < len; i++) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
                    values.charAt(rndm_method.nextInt(values.length()));

        }
        return password;
    }
}