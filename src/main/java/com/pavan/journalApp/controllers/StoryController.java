package com.pavan.journalApp.controllers;

import com.pavan.journalApp.dto.DateRangeDto;
import com.pavan.journalApp.models.Response;
import com.pavan.journalApp.models.Stories;
import com.pavan.journalApp.repositories.StoryRepo;
import com.pavan.journalApp.repositories.UserRepo;
import com.pavan.journalApp.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class StoryController {

    @Autowired
    StoryRepo storyRepo;

    @PostMapping("/addStory")
    public Response<Stories> addStory(@RequestBody Stories story) {

        Stories storyObj = storyRepo.save(story);
        return new Response<Stories>(Constants.SUCCESS, "Story added successfully", storyObj);

    }


    @PutMapping("/updateStory")
    public Response<Stories> updateStory(@RequestBody Stories story) {

        if (story.getId() == null)
            return new Response<>(Constants.FAILED, "Story id is missing", story);

        Stories storyObj = storyRepo.save(story);
        return new Response<Stories>(Constants.SUCCESS, "Story updated successfully", storyObj);

    }

    @GetMapping("/getAllStories")
    public Response<List<Stories>> getAllStoriesByEmail(@RequestParam("email") String email) {
        List<Stories> storiesList = storyRepo.findByUserEmail(email);
        return new Response<>(Constants.SUCCESS, "stories fetched successfully", storiesList);
    }

    @PostMapping("/getStoriesBetweenDateRange")
    public Response<List<Stories>> getAllStoriesInDateRange(@RequestBody DateRangeDto dateRangeDto) {

        List<Stories> storiesList = storyRepo.getStoriesBetweenDates(dateRangeDto.getFromDate(), dateRangeDto.getToDate(), dateRangeDto.getEmail());
        return new Response<>(Constants.SUCCESS, "stories fetched successfully", storiesList);

    }

    // update, delete, getStoryById, getAllStories


}
